from utils import *
import time
import flask
import arrow
from pymongo import MongoClient

mongua = MongoClient()


def timestamp():
    format = '%Y%H%M%S'
    value = time.localtime(int(time.time()))
    t = time.strftime(format, value)
    return t


def next_id(name):
    query = {
        'name': name,
    }
    update = {
        '$inc': {
            'seq': 1
        }
    }
    kwargs = {
        'query': query,
        'update': update,
        'upsert': True,
        'new': True,
    }
    # 存储数据的 id
    doc = mongua.db['data_id']
    # find_and_modify 是一个原子操作函数

    # 没有原子操作
    # A 查询拿到了 1
    # B 查询拿到了 1
    # B 先更新 数据变成了2
    # A 数据还是2

    # 有原子操作
    # A 查询拿到了 1
    # B 查询拿不到数据 会等待
    # A 更新 数据变成了2
    # B 查询拿到数据2


    new_id = doc.find_and_modify(**kwargs).get('seq')
    return new_id


class Mongo_ORM(object):
    @classmethod
    def valid_names(cls):
        names = [
            '_id',
            # (字段名, 类型, 值)
            ('id', int, -1),
            ('deleted', bool, False),
            ('created_time', int, 0),
            ('updated_time', int, 0),
        ]
        return names

    def __repr__(self):
        class_name = self.__class__.__name__
        properties = ('{0} = {1}'.format(k, v) for k, v in self.__dict__.items())
        return '<{0}: \n  {1}\n>'.format(class_name, '\n  '.join(properties))

    @classmethod
    def new(cls, form=None, **kwargs):
        """
        new 是给外部使用的函数
        """
        name = cls.__name__
        # 创建一个空对象
        m = cls()
        # 把定义的数据写入空对象, 未定义的数据输出错误
        names = cls.valid_names().copy()
        # 去掉 _id 这个特殊的字段
        names.remove('_id')
        if form is None:
            form = {}

        for f in names:
            k, t, v = f
            if k in form:
                setattr(m, k, t(form[k]))
            else:
                # 设置默认值
                setattr(m, k, v)
        # 处理额外的参数 kwargs
        for k, v in kwargs.items():
            if hasattr(m, k):
                setattr(m, k, v)
            else:
                raise KeyError
        # 写入默认数据
        m.id = next_id(name)
        # print('debug new id ', m.id)
        ts = int(time.time())
        m.created_time = ts
        m.updated_time = ts
        m.deleted = False
        m.save()
        return m

    # return model
    @classmethod
    def _new_with_bson(cls, bson):
        """
        这是给内部 all 这种函数使用的函数
        从 mongo 数据中恢复一个 model
        """
        m = cls()
        names = cls.valid_names().copy()
        # 去掉 _id 这个特殊的字段
        names.remove('_id')
        for f in names:
            k, t, v = f
            if k in bson:
                setattr(m, k, bson[k])
            else:
                # 设置默认值
                setattr(m, k, v)
        # 这一句必不可少，否则 bson 生成一个新的_id
        setattr(m, '_id', bson['_id'])
        return m

    @classmethod
    def all(cls):
        # 按照 id 升序排序
        # name = cls.__name__
        # ds = mongua.db[name].find()
        # l = [cls._new_with_bson(d) for d in ds]
        # return l
        return cls._find()

    # TODO, 还应该有一个函数 find(name, **kwargs)
    # find 类函数除了 _find() 都是废物
    @classmethod
    def _find(cls, **kwargs):
        """
        mongo 数据查询
        """
        name = cls.__name__
        kwargs['deleted'] = False
        # flag_sort = '__sort'
        # sort = kwargs.pop(flag_sort, None)
        ds = mongua.db[name].find(kwargs)
        # if sort == 'ascending'
        # if sort is not None:
        #     ds = ds.sort(sort)
        l = [cls._new_with_bson(d) for d in ds]
        return l

    @classmethod
    def find_by(cls, **kwargs):
        return cls.find_one(**kwargs)

    @classmethod
    def find_all(cls, **kwargs):
        return cls._find(**kwargs)

    @classmethod
    def find(cls, id):
        return cls.find_one(id=id)

    @classmethod
    def get(cls, id):
        return cls.find_one(id=id)

    @classmethod
    def find_one(cls, **kwargs):
        """
        """
        kwargs['deleted'] = False
        l = cls._find(**kwargs)
        # print('find one debug', kwargs, l)
        if len(l) > 0:
            return l[0]
        else:
            return None

    def save(self):
        name = self.__class__.__name__
        mongua.db[name].save(self.__dict__)

    def delete(self):
        name = self.__class__.__name__
        query = {
            'id': self.id,
        }
        values = {
            'deleted': True
        }
        # mongua.db[name].update_one(query, values)
        mongua.db[name].update(query, values)

    def blacklist(self):
        b = [
            '_id',
        ]
        return b

    def json(self):
        _dict = self.__dict__
        d = {k: v for k, v in _dict.items() if k not in self.blacklist()}
        return d

    @classmethod
    def find_page_id(cls, **kwargs):
        name = cls.__name__
        kwargs['deleted'] = False

        sk = kwargs.get('sk', -1)
        if sk == -1:
            sk = 0
        else:
            sk = kwargs.pop('sk', 0)

        li = kwargs.get('li', -1)
        if li == -1:
            li = 10
        else:
            li = kwargs.pop('li', 10)

        # ds = mongua.db[name].find(kwargs).skip(sk).limit(li).sort([('id', 1)])
        ds = mongua.db[name].find(kwargs).skip(sk).limit(li).sort([('id', -1)])
        l = [cls._new_with_bson(d) for d in ds]
        # log('find_page', l)
        return l

    def timestamp(self):
        # time 库
        # format = '%Y-%m-%d %H:%M:%S'
        # value = time.localtime(self.created_time)
        # t = time.strftime(format, value)
        # return t

        # arrow 库
        t = arrow.get(str(self.created_time)).to('+08:00').format('YYYY-MM-DD HH:mm:ss')
        return t


    def fromnow(self):
        now = int(time.time())
        delta = now - self.created_time

        if delta < 60:
            return '刚刚'

        elif delta >= 60 and delta < 60 * 60:
            value = delta // 60
            return str(value) + '分钟前'

        elif delta >= 60 * 60 and delta < 60 * 60 * 24:
            value = delta // (60 * 60)
            return str(value) + '小时前'

        elif delta >= 60 * 60 * 24 and delta < 60 * 60 * 24 * 30:
            value = delta // (60 * 60 * 24)
            return str(value) + '天前'

        elif delta >= 60 * 60 * 24 * 30:
            value = delta // (60 * 60 * 24 * 30)
            return str(value) + '个月前'
