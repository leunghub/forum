import time
from models.orm_mongo import Mongo_ORM

Model = Mongo_ORM
class Board(Model):

    @classmethod
    def valid_names(cls):
        names = super().valid_names()
        names = names + [
            ('title', str, ''),
        ]
        return names
