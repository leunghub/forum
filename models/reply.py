from models.orm_mongo import Mongo_ORM

Model = Mongo_ORM


class Reply(Model):
    @classmethod
    def valid_names(cls):
        names = super().valid_names()
        names = names + [
            ('content', str, ''),
            ('topic_id', int, 0),
            ('user_id', int, 0),
        ]
        return names

    def user(self):
        from .user import User
        u = User.find(self.user_id)
        return u

    # def topics(self, user_id):
    #     from .topic import Topic
    #     ms = Topic.find_all(reply_id=self.id)
    #     return ms
