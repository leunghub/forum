from flask import (
    render_template,
    request,
    redirect,
    session,
    url_for,
    Blueprint,
    make_response,
    send_from_directory,
)
from werkzeug.utils import secure_filename
from models.user import User
import os

from utils import *
from routes import *

main = Blueprint('index', __name__)


@main.route("/")
def index():
    return redirect('/topic')


@main.route('/register', methods=['GET'])
def register_get():
    return render_template('user/register.html')


@main.route("/register", methods=['POST'])
def register_post():
    form = request.form
    # 用类函数来判断
    u = User.register(form)
    if u is not None:
        session['user_id'] = u.id
        session.permanent = True
        return redirect(url_for('topic.index'))
    else:
        return redirect(url_for('.register_post'))


@main.route("/login", methods=['GET'])
def login_get():
    return render_template('user/login.html')


@main.route("/logout", methods=['GET'])
def logout():
    u = current_user()
    log('u', u)
    log('session', session)
    if u is not None:
        # session.pop(u.user_id)
        session.pop('user_id', '-1')
    else:
        pass
    return redirect(url_for('.index'))


@main.route("/login", methods=['POST'])
def login_post():
    form = request.form
    u = User.validate_login(form)
    if u is None:
        # 转到 topic.index 页面
        # return redirect(url_for('topic.index'))
        return redirect('/login')
    else:
        # session 中写入 user_id
        session['user_id'] = u.id
        # 设置 cookie 有效期为 永久
        session.permanent = True
        return redirect(url_for('topic.index'))


@main.route('/profile')
def profile():
    u = current_user()
    if u is None:
        return redirect(url_for('.index'))
    else:
        return render_template('profile.html', user=u)


def valid_suffix(suffix):
    valid_type = ['jpg', 'png', 'jpeg']
    return suffix in valid_type


@main.route('/image/add', methods=["POST"])
def add_img():
    u = current_user()

    # file 是一个上传的文件对象
    file = request.files['avatar']
    suffix = file.filename.split('.')[-1]
    if valid_suffix(suffix):
        # 上传的文件一定要用 secure_filename 函数过滤一下名字
        # ../../../../../../../root/.ssh/authorized_keys
        # filename = secure_filename(file.filename)
        # 2017/6/14/19/56/yiasduifhy289389f.png
        # import time
        # filename = str(time.time()) + filename
        filename = '{}.{}'.format(str(uuid.uuid4()), suffix)
        file.save(os.path.join('user_image', filename))
        # u.add_avatar(filename)
        u.user_image = '/uploads/' + filename
        u.save()

    return redirect(url_for("user.setting"))


@main.route('/test')
def test():
    return render_template('test.html')


# send_from_directory
# nginx 静态文件
@main.route("/uploads/<filename>")
def uploads(filename):
    return send_from_directory('user_image', filename)
