from flask import (
    Flask,
    render_template,
    request,
    redirect,
    url_for,
    Blueprint,
    abort,
)
from datetime import datetime

from utils import *
from routes import *

from models.topic import Topic
from models.board import Board

main = Blueprint('topic', __name__)


@main.route("/")
def index():
    # board_id = 2
    # board_id = int(request.args.get('board_id', -1))
    # if board_id == -1:
    #     ms = Topic.all()
    # else:
    #     ms = Topic.find_all(board_id=board_id)
    # token = str(uuid.uuid4())
    # u = current_user()
    # csrf_tokens['token'] = u.id
    # bs = Board.all()
    # return render_template("topic/index2.html", ms=ms, token=token, bs=bs, bid=board_id)

    u = current_user()
    page_id = int(request.args.get('page_id', 1))
    board_id = int(request.args.get('board_id', -1))
    bs = Board.all()
    # log('csrf', csrf_tokens)

    if board_id != -1:
        ts = Topic.find_all(board_id=board_id)
        ps = None
    else:
        skip = (page_id - 1) * 20
        # log('skip', skip)
        ts = Topic.find_page_id(sk=skip, li=20)
        # log('ts', ts)
        ts_all = Topic.all()
        if len(ts_all) % 20 != 0:
            p = int((len(ts_all) / 20) + 1)
        else:
            p = int((len(ts_all) / 20))
        ps = list(range(1, p + 1))

    return render_template("topic/index.html", user=u, bid=board_id, bs=bs, ts=ts, ps=ps)


@main.route('/search')
def search():
    q = request.args.get('q', -1)
    log('q', q)

    if q == '':
        return redirect(url_for('.index'))
    else:
        ts = Topic.find_all(title={'$regex': q})
        return render_template("topic/index.html", ts=ts, ps=None)


@main.route('/<int:id>')
def detail(id):
    u = current_user()
    m = Topic.get(id)
    # log('m', m)
    # 传递 topic 的所有 reply 到 页面中
    return render_template("topic/detail.html", user=u, topic=m)


@main.route('/add', methods=['POST'])
def add():
    u = current_user()
    form = request.form.to_dict()
    log('form', form)
    token = form.pop('token', -1)

    if token in csrf_tokens and csrf_tokens[token] == u.id:
        csrf_tokens.pop(token)
        if u is not None:
            m = Topic.new(form, user_id=u.id)
            return redirect(url_for('.detail', id=m.id))
    else:
        return url_for('.index')


@main.route('/delete')
def delete():
    id = int(request.args.get('id'))
    token = request.args.get('token')
    u = current_user()
    # 判断 token 是否是我们给的
    if token in csrf_tokens and csrf_tokens[token] == u.id:
        csrf_tokens.pop(token)
        if u is not None:
            # print('删除 topic 用户是', u, id)
            # Topic.delete(id)
            m = Topic.find_one(id=id)
            log('m', m)
            m.delete()
            return redirect(url_for('user.personal_index'))
        else:
            abort(403)
    else:
        abort(403)


@main.route("/new")
def new():
    u = current_user()
    board_id = int(request.args.get('board_id'))
    token = new_csrf_token()
    bs = Board.all()
    return render_template("topic/new.html", user=u, bs=bs, token=token, bid=board_id)
