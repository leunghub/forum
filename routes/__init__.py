from flask import session
import uuid

from models.user import User


def current_user():
    uid = session.get('user_id', '-1')
    u = User.find_by(id=uid)
    return u


global csrf_tokens
csrf_tokens = {}


def new_csrf_token():
    u = current_user()
    token = str(uuid.uuid4())
    csrf_tokens[token] = u.id
    return token
