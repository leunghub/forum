from flask import (
    render_template,
    request,
    redirect,
    url_for,
    Blueprint,
    abort,
)

from routes import *

from models.reply import Reply
from models.user import User
from models.topic import Topic
from models.board import Board
from utils import log

main = Blueprint('user', __name__)


@main.route('/')
def personal_index():
    u = current_user()
    token = new_csrf_token()
    ts = Topic.find_all(user_id=u.id)

    rs = Reply.find_all(user_id=u.id)
    ts_id = []
    ts2 = []

    for r in rs:
        if r.topic_id not in ts_id:
            ts_id.append(r.topic_id)

    for id in ts_id:
        ms = Topic.find_all(id=id)
        for m in ms:
            if m not in ts2:
                ts2.append(m)

    return render_template('user/index.html', user=u, ts=ts, ts2=ts2, token=token)


@main.route('/<username>')
def user_index(username):
    log(username)
    # u = None
    # if username != '':
    #     u = User.find_by(username=username)
    # if u is None:
    #     u = current_user()
    # log('u', u.id)
    # ts = Topic.find_all(user_id=u.id)
    # log('ts', ts)
    # rs = Reply.find_all(user_id=u.id)
    #
    # ts_id = []
    # ts2 = []
    #
    # for r in rs:
    #     if r.topic_id not in ts_id:
    #         ts_id.append(r.topic_id)
    #
    # for id in ts_id:
    #     ms = Topic.find_all(id=id)
    #     for m in ms:
    #         if m not in ts2:
    #             ts2.append(m)
    #
    # log('ts2', ts2)
    return render_template('user/index.html', ts=ts)
    # return render_template("user/index.html", user=u, ts=ts, ts2=ts2)


@main.route('/setting', methods=['GET'])
def setting_get():
    u = current_user()
    token = new_csrf_token()
    return render_template('user/setting.html', user=u, token=token)


@main.route('/setting', methods=['POST'])
def setting_post():
    u = current_user()
    form = request.form
    # log('form', form)
    token = form.get('token', -1)

    if token in csrf_tokens and csrf_tokens[token] == u.id:
        csrf_tokens.pop(token)
        log('form', form)
        if u is not None:
            password = request.form.get('password', -1)
            if password != -1 and password != '':
                u.password = u.salted_password(password)

            signature = request.form.get('signature', -1)
            if signature != -1:
                u.signature = signature

            if password != -1 or signature != -1:
                u.save()
        return redirect(url_for('.setting_get'))

    else:
        return abort(403)
